const axios = require('axios');
 const endpoint = 'https://api.openweather.org';
 const cityId = '6173331';
 const appId = 'a26b09d0cc77bd11090b0b48bf787172';
 const apiUrl = `${endpoint}/data/2.5/forecast?id=${cityId}&appid=${appId}`;
 
 async function getWeather(){
     const response = await axios.get(apiUrl);
     return response.data.list;
 }
 
 async function get(){
     const openWeather = await getWeather();
     openWeather.map(obj => {
      const dt = new Date(obj.dt*1000);
      return({
       dt: dt,
       dt_txt: dt.toLocalrString("en", {timeZone: "America/Vancouver"}),
         temp: obj.main.temp - 273.15,
         weather: obj.weather[0].description,
     });
 });
 }
module.exports =getWeather;