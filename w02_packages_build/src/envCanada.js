/**
 * Retrieve and display weather forecasts for Vancouver,BC
 **/
//  const endpoint = 'https://api.openweather.org';
//  const cityId = '6173331';
//  const appId = 'a26b09d0cc77bd11090b0b48bf787172';
//  const apiUrl = `${endpoint}/data/2.5/forecast?id=${cityId}&appid=${appId}`;
const axios = require('axios');
var xml2js = require('xml2js');

const apiurl = 'https://weather.gc.ca/rss/city/bc-74_e.xml'

async function getWeather(){
    const response = await axios.get(apiurl);
    const xml = response.data;
    const json = await xml2js.parseStringPromise(xml);
    return json.feed.entry;
}

async function get(){
    const json = await getWeather();    
    json.map(obj => obj.title[0])
}

getWeather()
.then(json => json.map(obj => obj.title[0]))
.then(title => title)
.then(console.log)
.catch(err => console.error(err));

module.exports = get;