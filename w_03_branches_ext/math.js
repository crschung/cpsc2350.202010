'use strict';

(function() { // closure
    const op1 = document.getElementById('op1');
    const op2 = document.getElementById('op2');
    const operator = document.getElementById('operator');
    const out = document.getElementById('numOut');

    function doMath() {
        let op1v = Number(op1.value);
        let op2v = Number(op2.value);
        switch (operator.value) {
            case '+':
            case '-':
            case '*':
            case '/':
                out.innerText = eval(`${op1v} ${operator.value} ${op2v}`);
                break;
            default:
                out.innerText = NaN;
        }
    }

    op1.addEventListener("input", doMath);
    op2.addEventListener("input", doMath);
    operator.addEventListener("change", doMath);
    doMath(); // fire the event to initialize
})();

