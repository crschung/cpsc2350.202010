


'use strict';

(function() { // closure
    const rgbPrefix = Object.freeze({ "red":"red", "green":"grn", "blue":"blu" });
    const out = document.getElementById('rectOut');
    const nums = Object.freeze(getInputElements('Num'));
    const slds = Object.freeze(getInputElements('Sld'));

    function onChange(source, dest) {
        // get the rgb values
        const rgb = getVals(source);
        // set other input to match
        Object.entries(rgb).forEach( ([k,v]) => dest[k].value = v );
        // set rectangle fill
        out.style.fill = `rgb(${rgb.red},${rgb.green},${rgb.blue})`;
    }

    // attach listeners
    function onChangeNum() { onChange(nums, slds); }
    function onChangeSld() { onChange(slds, nums); }
    Object.values(nums).forEach(v => v.addEventListener("input", onChangeNum));
    Object.values(slds).forEach(v => v.addEventListener("input", onChangeSld));

    // fire an initialization event
    onChangeNum();

    // helpers

    function getInputElements(suffix) {
        return valuesMap(rgbPrefix, (v) => document.getElementById(v+suffix) );
    }

    function getVals(from) {
        return valuesMap(from, (v) => Number(v.value));
    }

    // util

    function valuesMap(obj, fn) {
        return Object.fromEntries(Object.entries(obj).map( ([k,v]) => [k,fn(v,k)] ));
    }
})();

