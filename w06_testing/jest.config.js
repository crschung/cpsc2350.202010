module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  coverageDirectory: 'coverage',
};

/*
module.export = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  roots: ['src'],
  transform: { '^.+\\.tsx?$': 'ts-jest' },
  // jest #6051 #6674
  /*transform: {
    '^.+\\.tsx?$': '<rootDir>/node_modules/ts-jest',
  },
};
*/