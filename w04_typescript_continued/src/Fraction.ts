import gcd from './gcd';

export interface FractionVal {
  readonly num: number;
  readonly denom: number;
}

// ADT = Abstract Data Type
// defines the public interface of ADT Fraction
export default interface Fraction extends FractionVal {
  readonly num: number;
  readonly denom: number;
  
  /** Adds two fractions together
   * @param o the other fraction
   * @returns a new fraction which is the result of adding o to this in lowest terms
   */
  add(o: FractionVal): Fraction;
  subtract(o: FractionVal): Fraction;
  multiply(o: FractionVal): Fraction;
  divide(o: FractionVal): Fraction;
  inverse(): Fraction;
  equals(o: FractionVal): boolean;
}

export class FractionImpl implements Fraction {
  
  readonly num: number;
  readonly denom: number;
  
  constructor(num: number, denom: number) {
    const gcdVal = gcd(num, denom);
    this.num = num / gcdVal;
    this.denom = denom / gcdVal;
    if(this.denom < 0) { 
      this.num = this.num * -1;
      this.denom = this.denom * -1;
    }
  }
  
  add(o: FractionVal): FractionImpl {
    return new FractionImpl(
      this.num * o.denom + o.num * this.denom,
      this.denom * o.denom);
  }
  subtract(o: FractionVal): FractionImpl {
    return new FractionImpl(
      this.num * o.denom - o.num * this.denom,
      this.denom * o.denom);
  }
  multiply(o: FractionVal): FractionImpl {
    return new FractionImpl(
      this.num * o.num,
      this.denom * o.denom);
  }
  divide(o: Fraction): FractionImpl {
    return this.multiply(o.inverse());
  }
  inverse(): FractionImpl {
    return new FractionImpl(this.denom, this.num);
  }
  equals(o: FractionVal): boolean {
    return this.num === o.num && this.denom === o.denom;
  }
}