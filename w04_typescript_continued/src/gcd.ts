export default function gcd(n1: number, n2: number): number {
  let a = Math.abs(n1);
  let b = Math.abs(n2);
  if(isNaN(a) || isNaN(b)) throw new Error(`Illegal Arguments: n1:${n1}, n2:${n2}`);
  // https://en.wikipedia.org/wiki/Euclidean_algorithm
  while (a != b) {
    if (a > b) {
      a = a - b;
    } else {
      b = b - a;
    }
  }
  return a;
}
