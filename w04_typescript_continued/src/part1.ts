import Fraction, { FractionVal, FractionImpl } from './Fraction';

export default function p1() {
  let exp: FractionVal; // the expected value
  let act: Fraction; // the actual value
  let type: string; // test type

  function assert(exp: FractionVal, act: Fraction, type: string) {
    console.assert(exp.num === act.num && exp.denom === act.denom,
      't=%s exp=%o act=%o', type, exp, act);
  }

  console.log("Part 1 begin");

  // add
  type = 'add';
  exp = { num: 3, denom: 4 };
  // act = new FractionImpl(1,2).add(new FractionImpl(1,4));
  const op1 = new FractionImpl(1,2);
  const op2 = new FractionImpl(1,4);
  act = op1.add(op2);
  assert(exp, act, type);

  exp = { num: 3, denom: 2 };
  act = new FractionImpl(3,4).add(new FractionImpl(3,4));
  assert(exp, act, type);

  // subtract
  type = 'sub';
  exp = { num: 1, denom: 4 };
  act = new FractionImpl(1,2).subtract(new FractionImpl(1,4));
  assert(exp, act, type);

  exp = { num: -1, denom: 2 };
  act = new FractionImpl(1,4).subtract(new FractionImpl(3,4));
  assert(exp, act, type);

  // mult
  type = 'mult';
  exp = { num: 3, denom: 16 };
  act = new FractionImpl(1,4).multiply(new FractionImpl(3,4));
  assert(exp, act, type);

  exp = { num: 1, denom: 1 };
  act = new FractionImpl(2,3).multiply(new FractionImpl(3,2));
  assert(exp, act, type);

  //div
  type = 'div';
  exp = {num:4, denom:9};
  act = new FractionImpl(2,3).divide(new FractionImpl(3,2));
  assert(exp, act, type);

  exp = {num:-7, denom:6};
  act = new FractionImpl(2,3).divide(new FractionImpl(-4,7));
  assert(exp, act, type);

  // inverse
  type = 'inverse';
  exp = {num:3, denom:2};
  act = new FractionImpl(2,3).inverse();
  assert(exp, act, type);

  exp = {num:-3, denom:2};
  act = new FractionImpl(-3,2);
  assert(exp, act, type);

  // equals
  type = 'equals';
  exp = {num:1,denom:2};
  act = new FractionImpl(10,20);
  assert(exp, act, type);
  
  exp = {num:1,denom:2};
  act = new FractionImpl(-2,-4);
  assert(exp, act, type);
  
  exp = {num:1,denom:2};
  act = new FractionImpl(2,1);
  console.assert(!act.equals(exp));
  
  exp = {num:1,denom:2};
  act = new FractionImpl(-1,2);
  console.assert(!act.equals(exp));

  console.log("Part 1 end");
}
