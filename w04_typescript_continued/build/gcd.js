"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function gcd(n1, n2) {
    var a = Math.abs(n1);
    var b = Math.abs(n2);
    if (isNaN(a) || isNaN(b))
        throw new Error("Illegal Arguments: n1:" + n1 + ", n2:" + n2);
    // https://en.wikipedia.org/wiki/Euclidean_algorithm
    while (a != b) {
        if (a > b) {
            a = a - b;
        }
        else {
            b = b - a;
        }
    }
    return a;
}
exports.default = gcd;
//# sourceMappingURL=gcd.js.map