"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var gcd_1 = __importDefault(require("./gcd"));
var FractionImpl = /** @class */ (function () {
    function FractionImpl(num, denom) {
        var gcdVal = gcd_1.default(num, denom);
        this.num = num / gcdVal;
        this.denom = denom / gcdVal;
        if (this.denom < 0) {
            this.num = this.num * -1;
            this.denom = this.denom * -1;
        }
    }
    FractionImpl.prototype.add = function (o) {
        return new FractionImpl(this.num * o.denom + o.num * this.denom, this.denom * o.denom);
    };
    FractionImpl.prototype.subtract = function (o) {
        return new FractionImpl(this.num * o.denom - o.num * this.denom, this.denom * o.denom);
    };
    FractionImpl.prototype.multiply = function (o) {
        return new FractionImpl(this.num * o.num, this.denom * o.denom);
    };
    FractionImpl.prototype.divide = function (o) {
        return this.multiply(o.inverse());
    };
    FractionImpl.prototype.inverse = function () {
        return new FractionImpl(this.denom, this.num);
    };
    FractionImpl.prototype.equals = function (o) {
        return this.num === o.num && this.denom === o.denom;
    };
    return FractionImpl;
}());
exports.FractionImpl = FractionImpl;
//# sourceMappingURL=Fraction.js.map