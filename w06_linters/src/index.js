if (true) console.log('Hello world!'); // eslint-disable-line no-constant-condition

// eslint-disable-next-line no-constant-condition
if (true) {
  console.log('Hello world!');
}

/* eslint-disable no-constant-condition */
if (true) {
  console.log('Hello world!'); // eslint-disable-line no-constant-condition
}
/* eslint-enable */

let x = false;
if (x); // bad, didnt catch
console.log('false');
