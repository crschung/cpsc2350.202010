/* eslint-disable no-constant-condition */
if (true) console.log('Hello world!');
/* eslint-enable */

// eslint-disable-next-line no-constant-condition
if (true) console.log('Hello world!');

if (true) console.log('Hello world!'); // eslint-disable-line no-constant-condition

// if (true) console.log('Hello world!');
