class Point{
    x: number;
    y: number;
    
    constructor(x:number,y:number){
        this.x = x;
        this.y = y;
    }
    
    move(dx: number,dy: number): void{
        this.x+dx;
        this.y+dy;
    }
}

class Dimension{
    w: number;
    h: number;
    constructor(w: number,h: number){
        this.w = w;
        this.h = h;
    }
    
     get area(): number{
        return this.w * this.h;
    }
}

export default class Square{
    pos: Point;
    dim: Dimension;
    
    constructor(x: number,y: number,w: number,h: number){
       this.pos = new Point(x,y);
       this.dim = new Dimension(w,h);
    }
    get area(){
        return this.dim.area;
    }
  
    
    move(dx, dy){
        this.pos.move(dx,dy);
    }
}