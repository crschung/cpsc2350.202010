class Point {
    constuctor(x, y) {
        this.x = x;
        this.y = y;
    }
    move(dx, dy) {
        this.x + dx;
        this.y + dy;
    }
}
class Dimension {
    constuctor(w, h) {
        this.w = w;
        this.h = h;
    }
    get area() {
        return this.w * this.h;
    }
}
export default class Square {
    constructor(x, y, w, h) {
        this.pos = new Point;
        this.dim = new Dimension;
    }
    get area() {
        return this.dim.area;
    }
    move(dx, dy) {
        this.pos.move(dx, dy);
    }
}
