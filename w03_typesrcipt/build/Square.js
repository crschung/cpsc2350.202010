"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point = /** @class */ (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    Point.prototype.move = function (dx, dy) {
        this.x + dx;
        this.y + dy;
    };
    return Point;
}());
var Dimension = /** @class */ (function () {
    function Dimension(w, h) {
        this.w = w;
        this.h = h;
    }
    Object.defineProperty(Dimension.prototype, "area", {
        get: function () {
            return this.w * this.h;
        },
        enumerable: true,
        configurable: true
    });
    return Dimension;
}());
var Square = /** @class */ (function () {
    function Square(x, y, w, h) {
        this.pos = new Point(x, y);
        this.dim = new Dimension(w, h);
    }
    Object.defineProperty(Square.prototype, "area", {
        get: function () {
            return this.dim.area;
        },
        enumerable: true,
        configurable: true
    });
    Square.prototype.move = function (dx, dy) {
        this.pos.move(dx, dy);
    };
    return Square;
}());
exports.default = Square;
