'use strict'

function sum(...nums){
    let sum = 0;
    for(const n in nums){
        sum += nums[n];
    }
    return sum;
}

function product(...nums){
    return nums.reduce(
        (acc,cur)=>acc * cur,
        1);
}

module.exports = {
    sum: sum,
    product: product,
    PI: 22/7
}