import Person from './Person';
import PersonDb, { PersonDbStub } from './PersonDb';
import { UserIdFactoryStub } from './UserIdFactory';

export default class UserIdFactoryImpl extends UserIdFactoryStub {
  private readonly db: PersonDb = new PersonDbStub();

  makeByPerson(person: Person): Promise<string> {
    if (person.computeUserId) {
      return Promise.reject(new Error('already has'));
    }
    return this.makeByName(person.name.first, person.name.last);
  }

  private isPersonWithComputeIdExists(computeUserId: string): Promise<boolean> {
    return this.db
      .getPerson(computeUserId)
      .then(p => true)
      .catch(e => false);
  }

  async makeByName(firstName: string, lastName: string): Promise<string> {
    let userId = '';
    let counter = 1;
    let length = 8;
    do {
      let digits = (counter < 10?'0' : '')+ counter;
      userId = '';
      if (firstName) {
        userId = firstName[0];
      }
      userId += lastName;
      if(counter > 99){
        userId = userId.substring(0, 5) + counter++;
      }
      else{
        userId = userId.substring(0, 6) + (counter < 10?'0' : '')+ counter++;
      }
      if(counter > 999){
        throw Error(`exhaysted possible userids: ${userId}`)
      }
      userId = userId.toLowerCase();
    } while (await this.isPersonWithComputeIdExists(userId));
    return userId;
  }

  async makeById(id: number): Promise<string> {
    const p = await this.db.getPerson(id);
    return this.makeByPerson(p);
  }
}

