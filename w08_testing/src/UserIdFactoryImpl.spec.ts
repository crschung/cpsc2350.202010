import Person, { personTemplate, nameTemplate } from './Person';
import { PersonDbStub } from './PersonDb';
import UserIdFactory from './UserIdFactory';
import UserIdFactoryImpl from './UserIdFactoryImpl';

jest.mock('./PersonDb');
const mockPersonDbStub = PersonDbStub as jest.Mock<PersonDbStub>;

/* eslint-disable @typescript-eslint/unbound-method */

describe('UserIdFactoryImpl', () => {
  let mut: UserIdFactory;
  let mockInstance: jest.Mocked<PersonDbStub>;

  beforeEach(() => {
    mut = new UserIdFactoryImpl();
    mockInstance = mockPersonDbStub.mock.instances[0] as jest.Mocked<
      PersonDbStub
    >;
  });

  describe('makeByPerson', () => {
    test('+ve all fields, no computeUserId', async () => {
      expect.assertions(1);
      mockInstance.getPerson.mockRejectedValueOnce('');
      const p = {
        ...personTemplate,
        name: { ...nameTemplate, first: 'Alice', last: 'Liddell' },
      };
      await expect(mut.makeByPerson(p)).resolves.toBe('alidde01');
    });
    test('-ve already has computeUserId', async () => {
      expect.assertions(1);
      await expect(
        mut.makeByPerson({ ...personTemplate, computeUserId: 'x' })
      ).rejects.toThrowError(/already has/);
    });
  });

  describe('by name', () => {
    [
      ['Alice', 'Liddell', 'alidde01'], // truncates name
      ['Bob', 'Jay', 'bjay01'], // no truncate
      ['', 'Longlastname', 'longla01'], // no first name
      // TODO a03, think of a possible case not covered by above
    ].forEach(([first, last, expected]) => {
      test(`+ve ${first} ${last} => ${expected}`, async () => {
        expect.assertions(2);
        mockInstance.getPerson.mockRejectedValueOnce('');
        await expect(mut.makeByName(first, last)).resolves.toBe(expected);
        expect(mockInstance.getPerson).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('by id', () => {
    test('-ve no user with id', async () => {
      expect.assertions(2);
      const expErr = {};
      mockInstance.getPerson.mockRejectedValueOnce(expErr);
      await expect(mut.makeById(0)).rejects.toBe(expErr);
      expect(mockInstance.getPerson).toHaveBeenCalledTimes(1);
    });
    test('+ve', async () => {
      expect.assertions(2);
      const name = { ...nameTemplate, first: 'Alice', last: 'Liddell' };
      const p = {
        ...personTemplate,
        name,
        computeUserId: undefined,
      };
      mockInstance.getPerson.mockResolvedValueOnce(p);
      mockInstance.getPerson.mockRejectedValueOnce('');
      await expect(mut.makeById(0)).resolves.toBe('alidde01');
      expect(mockInstance.getPerson).toHaveBeenCalledTimes(2);
    });
  });

  describe('name collisions', () => {
    ['alidde01', 'alidde02', 'alidde99', 'alidd100', 'alidd999'].forEach(
      expected => {
        test(expected, async () => {
          expect.assertions(1);
          mockInstance.getPerson.mockImplementation(
            (a1: unknown, a2?: unknown): Promise<Person> => {
              const computeUserId = a1 as string;
              if (computeUserId === expected) {
                return Promise.reject();
              } 
              else if(computeUserId.length > 8){
                throw Error('computeUserId.length > 8');
                }
                else {
                return Promise.resolve(personTemplate);
              }
            }
          );
          await expect(mut.makeByName('Alice', 'Liddell')).resolves.toBe(
            expected
          );
        });
      }
    );
    test('01-999 already exists', async () => {
      expect.assertions(1);
       mockInstance.getPerson.mockResolvedValue(personTemplate);
      await expect(mut.makeByName('Alice', 'Liddell')).rejects.toThrowError();
      // TODO a03
      // does not make alid1000, rejects instead
      
    });
  });
});

