export interface Name {
  readonly first: string;
  readonly last: string;
  readonly middle: string[];
}
export function isName(o: unknown): o is Name {
  if (typeof o !== 'object' || o === null) return false;
  const n = o as Name; // cast so we can check the properties below
  return (
    typeof n.first === 'string' &&
    typeof n.last === 'string' &&
    n.middle instanceof Array &&
    n.middle.every((i: unknown) => typeof i === 'string')
  );
}
export const nameTemplate: Name = {
  first: 'Alice',
  last: 'Liddell',
  middle: [],
};

export default interface Person {
  readonly name: Name;
  readonly id: number;
  readonly computeUserId?: string;
}
export function isPerson(o: unknown): o is Person {
  if (typeof o !== 'object' || o === null) return false;
  const p = o as Person; // cast so we can check the properties below
  return (
    isName(p.name) &&
    typeof p.id === 'number' &&
    (typeof p.computeUserId === 'string' ||
      typeof p.computeUserId === 'undefined')
  );
}
export const personTemplate: Person = {
  name: {
    first: 'Alice',
    last: 'Liddell',
    middle: [],
  },
  id: 100123456,
  //computeUserId?: string
};
