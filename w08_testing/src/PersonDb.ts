import Person from './Person';

export default interface PersonDb {
  getPerson(id: number): Promise<Person>;
  getPerson(firstName: string, sirName: string): Promise<Person[]>;
  getPerson(computeUserId: string): Promise<Person>;
  getPerson(person: Person): Promise<Person>;
  updatePerson(p: Person): Promise<Person>;
}
/* istanbul ignore next */
export class PersonDbStub implements PersonDb {
  /* eslint-disable @typescript-eslint/no-unused-vars */
  getPerson<
    T = string | number | Person,
    U = T extends string ? string : never,
    R = U extends string ? Person[] : Person
  >(a1: T, a2?: U): Promise<R> {
    throw new Error('mock this implementation');
  }

  updatePerson(p: Person): Promise<Person> {
    throw new Error('mock this implementation');
  }
  /* eslint-enable */
}
