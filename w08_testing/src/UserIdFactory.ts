import Person from './Person';

export default interface UserIdFactory {
  makeByPerson(person: Person): Promise<string>;
  makeByName(firstName: string, lastName: string): Promise<string>;
  makeById(id: number): Promise<string>;
}
/* istanbul ignore next */
export class UserIdFactoryStub implements UserIdFactory {
  /* eslint-disable @typescript-eslint/no-unused-vars */
  makeByPerson(person: Person): Promise<string> {
    throw new Error();
  }
  makeByName(firstName: string, lastName: string): Promise<string> {
    throw new Error();
  }
  makeById(id: number): Promise<string> {
    throw new Error();
  }
  /* eslint-enable */
}

/*
make<T = string | number | Person, U = T extends string ? string : never>(
    arg1: T,
    arg2?: U
  ): Promise<string>
*/
